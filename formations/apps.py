from django.apps import AppConfig


class FormationsConfig(AppConfig):
    name = 'formations'
    verbose_name = '1-Formations'

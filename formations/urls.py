"""odex URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path

from . import views

urlpatterns = [
    path('toutes/', views.ListeFormations.as_view(), name='formations_liste'),
    path('detail/<str:pk>', views.DetailFormation.as_view(),name="formation_detail"),
    path('nouveau_devis_formation/<str:id_formation>', views.NouveauDevisFormation.as_view(),name="devis_formation_nouveau"),
    path('nouvelle_inscription_session/<str:id_session>', views.NouvelleInscriptionSession.as_view(),name="inscription_session_nouveau"),
]
# Generated by Django 2.2 on 2020-03-02 10:55

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import martor.models
import simple_history.models
import uuid


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('parametres', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Formation',
            fields=[
                ('deleted', models.DateTimeField(editable=False, null=True)),
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False, verbose_name='ID')),
                ('slug', models.SlugField(max_length=255, verbose_name='Slug')),
                ('code', models.CharField(max_length=255, unique=True, verbose_name='Code')),
                ('theme', models.CharField(max_length=255, verbose_name='Thème')),
                ('resume', martor.models.MartorField(blank=True, help_text='Résumé : Saisissez directement le texte en markdown', verbose_name='Résumé')),
                ('objectif', martor.models.MartorField(blank=True, help_text='Objectif : Saisissez directement le texte en markdown', verbose_name='Objectif')),
                ('programme', martor.models.MartorField(blank=True, help_text='Programme : Saisissez directement le texte en markdown', verbose_name='Programme')),
                ('prerequis', martor.models.MartorField(blank=True, help_text='Pré-requis : Saisissez directement le texte en markdown', verbose_name='Pré-requis')),
                ('public', martor.models.MartorField(blank=True, help_text='Public cible : Saisissez directement le texte en markdown', verbose_name='Public cible')),
                ('duree', models.IntegerField(verbose_name='Durée')),
                ('published', models.DateTimeField(auto_now_add=True, verbose_name='Publié le')),
                ('updated', models.DateTimeField(auto_now=True, verbose_name='Modifié le')),
                ('domaines', models.ManyToManyField(related_name='domaines', to='parametres.Domaine', verbose_name='Domaines')),
            ],
            options={
                'verbose_name_plural': '1.1-Formations',
                'db_table': 'formation',
                'ordering': ['theme'],
            },
        ),
        migrations.CreateModel(
            name='SessionFormation',
            fields=[
                ('deleted', models.DateTimeField(editable=False, null=True)),
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False, verbose_name='ID')),
                ('debut', models.DateField(verbose_name='Début')),
                ('fin', models.DateField(verbose_name='Fin')),
                ('prix', models.IntegerField(verbose_name='Prix')),
                ('published', models.DateTimeField(auto_now_add=True, verbose_name='Publié le')),
                ('updated', models.DateTimeField(auto_now=True, verbose_name='Modifié le')),
                ('devise', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='parametres.Devise', verbose_name='Devise')),
                ('formation', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='sessions', to='formations.Formation', verbose_name='Formation')),
                ('ville', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='parametres.Ville', verbose_name='Ville')),
            ],
            options={
                'verbose_name_plural': '1.2-Sessions',
                'db_table': 'session_formation',
                'ordering': ['debut'],
            },
        ),
        migrations.CreateModel(
            name='HistoricalSessionFormation',
            fields=[
                ('deleted', models.DateTimeField(editable=False, null=True)),
                ('id', models.UUIDField(db_index=True, default=uuid.uuid4, editable=False, verbose_name='ID')),
                ('debut', models.DateField(verbose_name='Début')),
                ('fin', models.DateField(verbose_name='Fin')),
                ('prix', models.IntegerField(verbose_name='Prix')),
                ('published', models.DateTimeField(blank=True, editable=False, verbose_name='Publié le')),
                ('updated', models.DateTimeField(blank=True, editable=False, verbose_name='Modifié le')),
                ('history_id', models.AutoField(primary_key=True, serialize=False)),
                ('history_date', models.DateTimeField()),
                ('history_change_reason', models.CharField(max_length=100, null=True)),
                ('history_type', models.CharField(choices=[('+', 'Created'), ('~', 'Changed'), ('-', 'Deleted')], max_length=1)),
                ('devise', models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='parametres.Devise', verbose_name='Devise')),
                ('formation', models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='formations.Formation', verbose_name='Formation')),
                ('history_user', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL)),
                ('ville', models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='parametres.Ville', verbose_name='Ville')),
            ],
            options={
                'verbose_name': 'historical session formation',
                'ordering': ('-history_date', '-history_id'),
                'get_latest_by': 'history_date',
            },
            bases=(simple_history.models.HistoricalChanges, models.Model),
        ),
        migrations.CreateModel(
            name='HistoricalFormation',
            fields=[
                ('deleted', models.DateTimeField(editable=False, null=True)),
                ('id', models.UUIDField(db_index=True, default=uuid.uuid4, editable=False, verbose_name='ID')),
                ('slug', models.SlugField(max_length=255, verbose_name='Slug')),
                ('code', models.CharField(db_index=True, max_length=255, verbose_name='Code')),
                ('theme', models.CharField(max_length=255, verbose_name='Thème')),
                ('resume', martor.models.MartorField(blank=True, help_text='Résumé : Saisissez directement le texte en markdown', verbose_name='Résumé')),
                ('objectif', martor.models.MartorField(blank=True, help_text='Objectif : Saisissez directement le texte en markdown', verbose_name='Objectif')),
                ('programme', martor.models.MartorField(blank=True, help_text='Programme : Saisissez directement le texte en markdown', verbose_name='Programme')),
                ('prerequis', martor.models.MartorField(blank=True, help_text='Pré-requis : Saisissez directement le texte en markdown', verbose_name='Pré-requis')),
                ('public', martor.models.MartorField(blank=True, help_text='Public cible : Saisissez directement le texte en markdown', verbose_name='Public cible')),
                ('duree', models.IntegerField(verbose_name='Durée')),
                ('published', models.DateTimeField(blank=True, editable=False, verbose_name='Publié le')),
                ('updated', models.DateTimeField(blank=True, editable=False, verbose_name='Modifié le')),
                ('history_id', models.AutoField(primary_key=True, serialize=False)),
                ('history_date', models.DateTimeField()),
                ('history_change_reason', models.CharField(max_length=100, null=True)),
                ('history_type', models.CharField(choices=[('+', 'Created'), ('~', 'Changed'), ('-', 'Deleted')], max_length=1)),
                ('history_user', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'historical formation',
                'ordering': ('-history_date', '-history_id'),
                'get_latest_by': 'history_date',
            },
            bases=(simple_history.models.HistoricalChanges, models.Model),
        ),
    ]

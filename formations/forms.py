from django import forms 
from .models import Formation, SessionFormation, InscriptionSession, DevisFormation

class InscriptionSessionForm(forms.ModelForm):

    class Meta:
        model = InscriptionSession
        fields = '__all__'
        exclude = ('session', )


class DevisFormationForm(forms.ModelForm):

    class Meta:
        model = DevisFormation
        fields = '__all__'
        exclude = ('formation', )

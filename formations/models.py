import uuid

from django.db import models
from django.urls import reverse

# Allow logical deleting (https://buildmedia.readthedocs.org/media/pdf/django-safedelete/latest/django-safedelete.pdf)
from safedelete.models import SafeDeleteModel, SOFT_DELETE, SOFT_DELETE_CASCADE
# Trace all actions on the database (https://django-simple-history.readthedocs.io/en/latest/)
from simple_history.models import HistoricalRecords
# Integrate Markdown WYSIWYG (https://github.com/agusmakmun/django-markdown-editor)
from martor.models import MartorField

from parametres.models import Domaine, Ville, Devise

# Create your models here.

#############################Formation identification###############################################


class Formation(SafeDeleteModel):
	""" A class to create a training instance. """

	_safedelete_policy = SOFT_DELETE_CASCADE
	id = models.UUIDField("ID", primary_key=True, default=uuid.uuid4, editable=False)
	slug = models.SlugField("Slug", max_length=255)
	code = models.CharField("Code", max_length=255, unique=True)
	theme = models.CharField("Thème", max_length=255)
	domaines = models.ManyToManyField(Domaine, verbose_name= "Domaines", related_name="domaines")
	resume = MartorField("Résumé", help_text="Résumé : Saisissez directement le texte en markdown", blank=True)
	objectif = MartorField("Objectif", help_text="Objectif : Saisissez directement le texte en markdown", blank=True)
	programme = MartorField("Programme", help_text="Programme : Saisissez directement le texte en markdown", blank=True)
	prerequis = MartorField("Pré-requis", help_text="Pré-requis : Saisissez directement le texte en markdown", blank=True)
	public = MartorField("Public cible", help_text="Public cible : Saisissez directement le texte en markdown", blank=True)
	duree = models.IntegerField("Durée")
	published = models.DateTimeField("Publié le", auto_now_add=True)
	updated = models.DateTimeField("Modifié le", auto_now=True)
	log = HistoricalRecords()

	class Meta:

		db_table = 'formation'
		verbose_name_plural = '1.1-Formations'
		ordering = ['theme']

	def __str__(self):

		return f"{self.theme}"


class SessionFormation(SafeDeleteModel):
	""" A class to create a session of training instance. """

	_safedelete_policy = SOFT_DELETE_CASCADE
	id = models.UUIDField("ID", primary_key=True, default=uuid.uuid4, editable=False)
	formation = models.ForeignKey(Formation, verbose_name="Formation", on_delete=models.CASCADE, related_name="sessions")
	ville = models.ForeignKey(Ville, verbose_name="Ville", on_delete=models.CASCADE)
	debut = models.DateField("Début")
	fin = models.DateField("Fin")
	prix = models.IntegerField("Prix")
	devise = models.ForeignKey(Devise, verbose_name="Devise", on_delete=models.CASCADE)
	inscriptions_ouvertes = models.BooleanField("Inscription Ouverte ?", default=True)
	published = models.DateTimeField("Publié le", auto_now_add=True)
	updated = models.DateTimeField("Modifié le", auto_now=True)
	log = HistoricalRecords()

	class Meta:

		db_table = 'session_formation'
		verbose_name_plural = '1.2-Sessions'
		ordering = ['debut']

	def __str__(self):

		return f"{self.formation.theme} / {self.debut} / {self.ville}"


class InscriptionSession(SafeDeleteModel):
	""" A class to create a registration instance to a session of training . """

	_safedelete_policy = SOFT_DELETE_CASCADE
	id = models.UUIDField("ID", primary_key=True, default=uuid.uuid4, editable=False)
	session = models.ForeignKey(SessionFormation, verbose_name="Session", on_delete=models.CASCADE, related_name="inscriptions")
	nom = models.CharField("Nom", max_length=255)
	prenoms = models.CharField("Prénoms", max_length=255)
	tel = models.CharField("Téléphone", max_length=255)
	email = models.EmailField("Email", max_length=255)
	message = models.TextField("Message", blank=True)
	published = models.DateTimeField("Publié le", auto_now_add=True)
	updated = models.DateTimeField("Modifié le", auto_now=True)
	log = HistoricalRecords()

	class Meta:

		db_table = 'inscription_session'
		verbose_name_plural = '1.3-Inscriptions'
		ordering = ['published']

	def __str__(self):

		return f"{self.nom} {self.prenoms} / {self.session.formation.theme} / {self.session.debut}"

	def get_absolute_url(self):

		return reverse('envoi_mail_succes', kwargs={})


class DevisFormation(SafeDeleteModel):
	""" A class to create a quote instance for of training . """

	_safedelete_policy = SOFT_DELETE_CASCADE
	id = models.UUIDField("ID", primary_key=True, default=uuid.uuid4, editable=False)
	formation = models.ForeignKey(Formation, verbose_name="Formation", on_delete=models.CASCADE, related_name="devis")
	nom = models.CharField("Nom", max_length=255)
	prenoms = models.CharField("Prénoms", max_length=255)
	tel = models.CharField("Téléphone", max_length=255)
	email = models.EmailField("Email", max_length=255)
	entreprise = models.CharField("Entreprise", max_length=255)
	pays = models.CharField("Pays", max_length=255)
	nombre = models.IntegerField("Nombre de salariés à former")
	projet = models.FileField("Projet de formation", upload_to='projets-formations/', max_length=255, blank=True)
	message = models.TextField("Message", blank=True)
	published = models.DateTimeField("Publié le", auto_now_add=True)
	updated = models.DateTimeField("Modifié le", auto_now=True)
	log = HistoricalRecords()

	class Meta:

		db_table = 'devis_formation'
		verbose_name_plural = '1.4-Demandes de devis'
		ordering = ['published']

	def __str__(self):

		return f"{self.nom} {self.prenoms} / {self.entreprise} / {self.formation.theme}"

	def get_absolute_url(self):

		return reverse('envoi_mail_succes', kwargs={})



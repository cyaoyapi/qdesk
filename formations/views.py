import datetime

from django.shortcuts import render
from django.core.mail import EmailMessage
from django.views.generic import ListView, DetailView, CreateView
from .models import Formation, SessionFormation, InscriptionSession, DevisFormation
from .forms import InscriptionSessionForm, DevisFormationForm

# Create your views here.

class ListeFormations(ListView):

	""" Generic view to list trainings"""

	model = Formation
	context_object_name = "formations"
	template_name = "formations/liste_formations.html"

class DetailFormation(DetailView):

	""" Generic view to display details of trainings"""

	model = Formation
	context_object_name = "formation"
	template_name = "formations/detail_formation.html"

	def get_context_data(self, **kwargs):

		context = super(DetailFormation, self).get_context_data(**kwargs)
		formation = Formation.objects.get(pk=self.kwargs['pk'])
		sessions = SessionFormation.objects.filter(formation=formation, debut__lt=datetime.date.today())
		session_ouverte_existe = False
		for session in sessions:
			if session.inscriptions_ouvertes:
				session_ouverte_existe = True
		context['session_ouverte_existe'] = session_ouverte_existe
		return context


class NouvelleInscriptionSession(CreateView):

	""" Generic view to create a registration to a session of training. """

	model = InscriptionSession
	template_name = "formations/nouveau_inscription_session.html"
	form_class = InscriptionSessionForm

	def get_context_data(self, **kwargs):

		context = super(NouvelleInscriptionSession, self).get_context_data(**kwargs)
		if 'id_session' in self.kwargs:
			context['session'] = SessionFormation.objects.get(pk=self.kwargs['id_session'])

		return context


	def form_valid(self, form):

		session_id = self.request.POST['session']
		session = SessionFormation.objects.get(id=session_id)
		form.instance.session = session

		message_to_odex_content = f"<p><strong>Nom</strong> : {self.request.POST['nom']}</p> \
			<p><strong>Prénoms</strong> : {self.request.POST['prenoms']}</p> \
			<p><strong>Email</strong> : {self.request.POST['email']}</p> \
			<p><strong>Tel</strong> : {self.request.POST['tel']}</p> \
			<p><strong>Message</strong> : {self.request.POST['message']}</p>"

		message_to_customer_content = f"<p>Bonjour {self.request.POST['nom']},</p> \
			<p>Votre préinscription à la session du <em>{session.formation.theme}</em> à <em>{session.ville.nom}</em> \
			de formation la formation <em>{session.formation.theme}</em> a été reçue avec succès.</p> \
			<p>Nous vous reviendrons dans un bref délai de 48 heures maximum.</p> \
			<p>Merci!</p><p>Odex Consulting</p>"


		message_to_odex = EmailMessage(
			f'Nouvelle Inscription - {session.formation.theme} / {session.debut} / {session.ville.nom}',
			message_to_odex_content, 
			self.request.POST['email'], 
			["test@odex-consulting.com"],
			['cyapiprisca@gmail.com'])
		message_to_odex.content_subtype = "html"  
		message_to_odex.send()	

		message_to_customer = EmailMessage(
    		f'Inscription formation - odex-consulting.com - {session.formation.theme} / {session.debut} / {session.ville.nom}',
			message_to_customer_content, 
			'test@odex-consulting.com', 
			[self.request.POST['email']])
		message_to_customer.content_subtype = "html"
		message_to_customer.send()	

		return super(NouvelleInscriptionSession, self).form_valid(form)



class NouveauDevisFormation(CreateView):

	""" Generic view to create a quote of training. """

	model = DevisFormationForm
	template_name = "formations/nouveau_devis_formation.html"
	form_class = DevisFormationForm

	def get_context_data(self, **kwargs):

		context = super(NouveauDevisFormation, self).get_context_data(**kwargs)
		if 'id_formation' in self.kwargs:
			context['formation'] = Formation.objects.get(pk=self.kwargs['id_formation'])

		return context


	def form_valid(self, form):

		formation_id = self.request.POST['formation']
		formation = Formation.objects.get(id=formation_id)
		form.instance.formation = formation

		message_to_odex_content = f"<p><strong>Nom</strong> : {self.request.POST['nom']}</p> \
			<p><strong>Prénoms</strong> : {self.request.POST['prenoms']}</p> \
			<p><strong>Email</strong> : {self.request.POST['email']}</p> \
			<p><strong>Tel</strong> : {self.request.POST['tel']}</p> \
			<p><strong>Entreprise</strong> : {self.request.POST['entreprise']}</p> \
			<p><strong>Pays</strong> : {self.request.POST['pays']}</p> \
			<p><strong>Nombre de personnes à former </strong> : {self.request.POST['nombre']}</p> \
			<p><strong>Message</strong> : {self.request.POST['message']}</p>"

		message_to_customer_content = f"<p>Bonjour {self.request.POST['nom']},</p> \
			<p>Votre demande de devis pour la formation <em>{formation.theme}</em> a été reçue avec succès.</p> \
			<p>Nous vous reviendrons dans un bref délai de 48 heures maximum.</p> \
			<p>Merci!</p><p>Odex Consulting</p>"

		message_to_odex = EmailMessage(
			f"Demande de devis de formation - {self.request.POST['nom']} / {self.request.POST['entreprise']} - {formation.theme}",
			message_to_odex_content, 
			self.request.POST['email'], 
			["test@odex-consulting.com"],
			['cyapiprisca@gmail.com'])
		if len(self.request.FILES) != 0:
			message_to_odex.attach(self.request.FILES["projet"].name, self.request.FILES["projet"].read(), self.request.FILES["projet"].content_type)
		message_to_odex.content_subtype = "html"  
		message_to_odex.send()	

		message_to_customer = EmailMessage(
    		f"Votre demande de devis de formation - odex-consulting.com - {formation.theme}",
			message_to_customer_content, 
			'test@odex-consulting.com', 
			[self.request.POST['email']])
		message_to_customer.content_subtype = "html"
		message_to_customer.send()	
		
		return super(NouveauDevisFormation, self).form_valid(form)

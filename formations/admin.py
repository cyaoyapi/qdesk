from django.contrib import admin
from django.db import models
from django import forms

# Allow logical deleting (https://buildmedia.readthedocs.org/media/pdf/django-safedelete/latest/django-safedelete.pdf)
from simple_history.admin import SimpleHistoryAdmin
# Integrate Markdown WYSIWYG (https://github.com/agusmakmun/django-markdown-editor)
from martor.widgets import AdminMartorWidget

from .models import Formation, SessionFormation, InscriptionSession, DevisFormation

# Register your models here.

class FormationModelAdmin(admin.ModelAdmin):
    formfield_overrides = {
        models.TextField: {'widget': AdminMartorWidget},
    }

    prepopulated_fields = {"slug": ("theme",)}

class SessionFormationModelAdmin(admin.ModelAdmin):
    formfield_overrides = {
        models.TextField: {'widget': AdminMartorWidget},
    }

class InscriptionSessionModelAdmin(admin.ModelAdmin):
    formfield_overrides = {
        models.TextField: {'widget': AdminMartorWidget},
    }

class DevisFormationModelAdmin(admin.ModelAdmin):
    formfield_overrides = {
        models.TextField: {'widget': AdminMartorWidget},
    }

#######################################################################################
admin.site.register(Formation, FormationModelAdmin)
admin.site.register(SessionFormation, SessionFormationModelAdmin)
admin.site.register(InscriptionSession, InscriptionSessionModelAdmin)
admin.site.register(DevisFormation, DevisFormationModelAdmin)

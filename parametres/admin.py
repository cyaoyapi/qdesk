from django.contrib import admin
from django.db import models
from django import forms

from .models import Domaine, Pays, Ville, Devise

# Register your models here.



#######################################################################################
admin.site.register(Domaine)
admin.site.register(Pays)
admin.site.register(Ville)
admin.site.register(Devise)

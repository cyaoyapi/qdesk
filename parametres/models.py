import uuid

from django.db import models

# Allow logical deleting (https://buildmedia.readthedocs.org/media/pdf/django-safedelete/latest/django-safedelete.pdf)
from safedelete.models import SafeDeleteModel, SOFT_DELETE, SOFT_DELETE_CASCADE
# Trace all actions on the database (https://django-simple-history.readthedocs.io/en/latest/)
from simple_history.models import HistoricalRecords
# Integrate Markdown WYSIWYG (https://github.com/agusmakmun/django-markdown-editor)
from martor.models import MartorField
# Create your models here.

#############################Formation identification###############################################


class Domaine(SafeDeleteModel):
	""" A class to create a domaine instance. """

	_safedelete_policy = SOFT_DELETE_CASCADE
	id = models.UUIDField("ID", primary_key=True, default=uuid.uuid4, editable=False)
	nom = models.CharField("Nom", max_length=255)
	published = models.DateTimeField("Publié le", auto_now_add=True)
	updated = models.DateTimeField("Modifié le", auto_now=True)
	log = HistoricalRecords()

	class Meta:

		db_table = 'domaine'
		verbose_name_plural = '0.1-Domaines'
		ordering = ['nom']

	def __str__(self):

		return f"{self.nom}"


class Pays(SafeDeleteModel):
	""" A class to create a country instance. """

	_safedelete_policy = SOFT_DELETE_CASCADE
	id = models.UUIDField("ID", primary_key=True, default=uuid.uuid4, editable=False)
	nom = models.CharField("Nom", max_length=255)
	published = models.DateTimeField("Publié le", auto_now_add=True)
	updated = models.DateTimeField("Modifié le", auto_now=True)
	log = HistoricalRecords()

	class Meta:

		db_table = 'pays'
		verbose_name_plural = '0.2-Pays'
		ordering = ['nom']

	def __str__(self):

		return f"{self.nom}"



class Ville(SafeDeleteModel):
	""" A class to create a city instance. """

	_safedelete_policy = SOFT_DELETE_CASCADE
	id = models.UUIDField("ID", primary_key=True, default=uuid.uuid4, editable=False)
	nom = models.CharField("Nom", max_length=255)
	pays = models.ForeignKey(Pays, verbose_name="Pays", related_name="ville", on_delete=models.CASCADE)
	published = models.DateTimeField("Publié le", auto_now_add=True)
	updated = models.DateTimeField("Modifié le", auto_now=True)
	log = HistoricalRecords()

	class Meta:

		db_table = 'ville'
		verbose_name_plural = '0.3-Villes'
		ordering = ['nom']

	def __str__(self):

		return f"{self.nom}"


class Devise(SafeDeleteModel):
	""" A class to create a currency instance. """

	_safedelete_policy = SOFT_DELETE_CASCADE
	id = models.UUIDField("ID", primary_key=True, default=uuid.uuid4, editable=False)
	nom = models.CharField("Nom", max_length=255)
	symbole = models.CharField("Symbole", max_length=255)
	published = models.DateTimeField("Publié le", auto_now_add=True)
	updated = models.DateTimeField("Modifié le", auto_now=True)
	log = HistoricalRecords()

	class Meta:

		db_table = 'devise'
		verbose_name_plural = '0.4-Devises'
		ordering = ['nom']

	def __str__(self):

		return f"{self.nom}"

$(document).ready( function () {
    $('#grid').DataTable({

    	dom: "<'row'<'col-sm-3'l><'col-sm-6 text-center'B><'col-sm-3'f>>" +
    "<'row'<'col-sm-12'tr>>" +
    "<'row'<'col-sm-5'i><'col-sm-7'p>>",
        "lengthMenu" : [[15, 30, 50, 100, -1], [15, 30, 50, 100, "Tout"]],
    	buttons: [

    		{"extend": 'print', "text":'<span><i class="fa fa-print" aria-hidden="true"></i>  Imprimer&nbsp;</span>',"className": 'btn btn-default btn-xs export' },
    		{"extend": 'csv', "text":'<span><i class="fa fa-file-text-o" aria-hidden="true"></i> CSV &nbsp;</span>',"className": 'btn btn-default btn-xs export' },
    		{"extend": 'excel', "text":'<span><i class="fa fa-file-excel-o" aria-hidden="true"></i> Excel &nbsp;</span>',"className": 'btn btn-default btn-xs export' },
    		{"extend": 'pdf', "text":'<span><i class="fa fa-file-pdf-o" aria-hidden="true"></i> PDF &nbsp;</span>',"className": 'btn btn-default btn-xs export' },
    	],

    	language: {
            processing:     "Traitement en cours...",
            search:         "Recherche basique ",
            lengthMenu:     "Afficher _MENU_ &eacute;l&eacute;ments",
            info:           "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty:      "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix:    "",
            loadingRecords: "Chargement en cours...",
            zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable:     "Aucune donnée disponible dans le tableau",
            paginate: {
                first:      "Premier",
                previous:   "Pr&eacute;c&eacute;dent",
                next:       "Suivant",
                last:       "Dernier"
            },
            aria: {
                sortAscending:  ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        }
        
    });


    $('table.display').DataTable({

    	dom: "<'row'<'col-sm-3'l><'col-sm-6 text-center'B><'col-sm-3'f>>" +
    "<'row'<'col-sm-12'tr>>" +
    "<'row'<'col-sm-5'i><'col-sm-7'p>>",
        "lengthMenu" : [[15, 30, 50, 100, -1], [15, 30, 50, 100, "Tout"]],
    	buttons: [

    		{"extend": 'print', "text":'<span><i class="fa fa-print" aria-hidden="true"></i>  Imprimer&nbsp;</span>',"className": 'btn btn-default btn-xs export' },
    		{"extend": 'csv', "text":'<span><i class="fa fa-file-text-o" aria-hidden="true"></i> CSV &nbsp;</span>',"className": 'btn btn-default btn-xs export' },
    		{"extend": 'excel', "text":'<span><i class="fa fa-file-excel-o" aria-hidden="true"></i> Excel &nbsp;</span>',"className": 'btn btn-default btn-xs export' },
    		{"extend": 'pdf', "text":'<span><i class="fa fa-file-pdf-o" aria-hidden="true"></i> PDF &nbsp;</span>',"className": 'btn btn-default btn-xs export' },
    	],

    	language: {
            processing:     "Traitement en cours...",
            search:         "Recherche basique ",
            lengthMenu:     "Afficher _MENU_ &eacute;l&eacute;ments",
            info:           "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty:      "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix:    "",
            loadingRecords: "Chargement en cours...",
            zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable:     "Aucune donnée disponible dans le tableau",
            paginate: {
                first:      "Premier",
                previous:   "Pr&eacute;c&eacute;dent",
                next:       "Suivant",
                last:       "Dernier"
            },
            aria: {
                sortAscending:  ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        }
        
    });
    
} );
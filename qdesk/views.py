from django.shortcuts import render

def home(request):
    return render(request, 'index.html', {})

def firm(request):
    return render(request, 'le-cabinet.html', {})

def diagnostic(request):
    return render(request, 'diagnostic.html', {})

def assistance(request):
    return render(request, 'assistance.html', {})

def audit(request):
    return render(request, 'audit.html', {})

def construction(request):
    return render(request, 'construction.html', {})

def message_envoye_succes(request):
    return render(request, 'message_envoye_succes.html', {})

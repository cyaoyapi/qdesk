"""qdesk URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include

from . import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.home, name='home'),
    path('le-cabinet', views.firm, name='firm'),
    path('diagnostic', views.diagnostic, name='diagnostic'),
    path('assistance', views.assistance, name='assistance'),
    path('audit', views.audit, name='audit'),
    path('construction', views.construction, name='construction'),
    path('message_envoye_succes', views.message_envoye_succes, name='envoi_mail_succes'),
    path('formation/', include('formations.urls')),
]

from .base import *

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'f!hap7sff#f@8$1iix@(%d4f=88swwetxkhbq=%^x)ga2eowbs'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

# Database
# https://docs.djangoproject.com/en/2.2/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

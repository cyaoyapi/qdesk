from .base import *

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

# SECURITY WARNING: keep the secret key used in production secret!
from django.core.exceptions import ImproperlyConfigured

def get_env_variable(var_name):
    try:
        return os.environ[var_name]
    except KeyError:
        error_msg = "Set the %s environment variable" % var_name
        raise ImproperlyConfigured(error_msg)
 
SECRET_KEY = get_env_variable('SECRET_KEY')

STATIC_ROOT='/home/bcit/apps/staticfiles_store/qdesk'

ALLOWED_HOSTS = ['qdesk-consulting.com']

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'OPTIONS': {
            'read_default_file': '/home/bcit/apps/bdconf/qdesk.cnf',
        },
    }
}

### Email config

EMAIL_HOST = 'smtp-bcit.alwaysdata.net'
EMAIL_PORT = 587
EMAIL_USE_TLS = True